﻿function LoadGame(key)
{
    var games = store(key);
    playerNames = [];
    for(var pl in games[1])
    {
        if(games[1][pl].name.indexOf('bank') == -1 && games[1][pl].name.length)
        {
            playerNames.push(games[1][pl].name);
        }
    }
    document.getElementById("playernumber").value = playerNames.length; 
    game_ns.setup();

    player = games[1];
    square = games[0];
    turn = games[2];
    for(var x in player)
    {
        player[x].pay = payFn;
    }
    doublecount = games[3];  
}
function EraseGame(key)
{
    store(key, null);
    var keyList = store("keys");

    for(var i = 0; i < keyList.length; i++)
    {
        if(keyList[i] == key)
        {
            keyList.splice(i, 1);
            break;
        }
    }             
    store("keys", keyList);       
    $('.'+key).remove();
}
function SaveGame()
{
    var dt = new Date();

        var key = "savedGame"+dt.getMonth() + "-" + dt.getDate() + "-" + dt.getFullYear() + "-" 
            + dt.getHours() + "-" + dt.getMinutes() + "-" + dt.getSeconds();
          
        store(key, 
            [square, player, turn, doublecount]);
        var keyList = store("keys");
        if(!keyList)
        {
            keyList = [];
        }
        keyList.push(key);
        store('keys',keyList);
        alert('Game Saved');    
}
$(document).ready(function(){   
    var kList = store("keys");
    $('#savedGames').html('');
    for(var kIndex in kList)
    {
        if(kList[kIndex] && kList[kIndex].toString().indexOf('function') === -1)
        {
            $('#savedGames').append('<li class=\''+kList[kIndex]+'\'>'+kList[kIndex]);
            $('#savedGames').append('<input type=\'button\' class=\'btn btn-default '+kList[kIndex]+'\' value=\'Load Game\' onclick=\'LoadGame(\"'+ kList[kIndex]+'\");\' />');
            $('#savedGames').append('<input type=\'button\' class=\'btn btn-default '+kList[kIndex]+'\'  value=\'Delete Game\' onclick=\'EraseGame(\"'+kList[kIndex]+'\");\' /> </li>');
        }
    }

});




